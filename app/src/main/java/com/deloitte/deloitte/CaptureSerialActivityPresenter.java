package com.deloitte.deloitte;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.deloitte.deloitte.model.RestClient;
import com.deloitte.deloitte.model.pojo.CheckSerial;
import com.deloitte.deloitte.model.pojo.loginuser.UserLogin;
import com.deloitte.deloitte.model.pojo.readbrands.ReadBrands;
import com.deloitte.deloitte.utils.ConnectionDetector;
import com.deloitte.deloitte.utils.Constants;
import com.deloitte.deloitte.utils.PreferenceStorage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rats on 14/6/2017.
 */

public class CaptureSerialActivityPresenter {

    private AppCompatActivity context;
    private CheckSerial checkSerial;
    private PreferenceStorage preferenceStorage;

    CaptureSerialActivityPresenter(AppCompatActivity context) {
        this.context = context;
        preferenceStorage = PreferenceStorage.getInstance();
    }

    void postCheckSerial(String brandId, String serialNumber, String location) {
        if (ConnectionDetector.isConnectedToInternet()) {
            Call<CheckSerial> call = RestClient.post().postCheckSerial(preferenceStorage.getStringData("api_token"), brandId, serialNumber, location);
            call.enqueue(new Callback<CheckSerial>() {
                @Override
                public void onResponse(Call<CheckSerial> call, Response<CheckSerial> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            checkSerial = response.body();
                            if (checkSerial.getCode().equals("0")) {
                                Toast.makeText(context, checkSerial.getMessage(), Toast.LENGTH_LONG).show();
                            } else {
                                publish();
                            }
                        }
                    } else {
                        Toast.makeText(context, Constants.SERVER_NOT_RESPONDING, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<CheckSerial> call, Throwable t) {
                    Toast.makeText(context, Constants.SERVER_NOT_RESPONDING, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
        }
    }

    private void publish() {
        if (context instanceof CaptureSerialActivity) {
            ((CaptureSerialActivity) context).checkSerialResult(checkSerial);
        }
    }
}
