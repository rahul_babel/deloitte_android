package com.deloitte.deloitte;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;

public class ResultActivity extends AppCompatActivity {

    AppCompatTextView result;
    AppCompatTextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        initialize();
    }

    private void initialize() {
        String res = getIntent().getStringExtra("result");
        String code = getIntent().getStringExtra("code");
        result = (AppCompatTextView) findViewById(R.id.result);
        message = (AppCompatTextView) findViewById(R.id.message);
        result.setText(code);
        message.setText(res);
    }
}
