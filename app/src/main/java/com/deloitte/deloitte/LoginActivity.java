package com.deloitte.deloitte;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.deloitte.deloitte.model.pojo.UserSignUp;
import com.deloitte.deloitte.model.pojo.loginuser.UserLogin;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    SignUpFragment signUpFragment;
    LoginFragment loginFragment;
    ForgotPasswordFragment forgotPasswordFragment;
    LoginActivityPresenter loginActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialize();
    }

    protected void initialize() {
        loginActivityPresenter = new LoginActivityPresenter(this);
        loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.login_fragment);
        signUpFragment = (SignUpFragment) getSupportFragmentManager().findFragmentById(R.id.sign_up_fragment);
        forgotPasswordFragment = (ForgotPasswordFragment) getSupportFragmentManager().findFragmentById(R.id.forgot_password_fragment);
    }

    protected void onClickSignUp() {
        loginFragment.getView().setVisibility(View.GONE);
        signUpFragment.getView().setVisibility(View.VISIBLE);
    }

    protected void onClickLogin() {
        loginFragment.getView().setVisibility(View.VISIBLE);
    }

    protected void onClickForgotPassword() {
        loginFragment.getView().setVisibility(View.GONE);
        signUpFragment.getView().setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (loginFragment.getView().getVisibility() == View.GONE) {
            loginFragment.getView().setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }

    public LoginActivityPresenter getPresenter() {
        return loginActivityPresenter;
    }

    public void login(UserLogin userLogin) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("userLogin", userLogin);
        startActivity(intent);
        finish();
    }

    public void signUp(UserSignUp userSignUp) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
//    private void showProgress(final boolean show) {
//        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
//        // for very easy animations. If available, use these APIs to fade-in
//        // the progress spinner.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
//            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
//
//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
//                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//                }
//            });
//
//            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//            mProgressView.animate().setDuration(shortAnimTime).alpha(
//                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//                }
//            });
//        } else {
//            // The ViewPropertyAnimator APIs are not available, so simply show
//            // and hide the relevant UI components.
//            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//        }
//    }
}

