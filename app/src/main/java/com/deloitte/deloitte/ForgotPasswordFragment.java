package com.deloitte.deloitte;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Rats on 10/6/2017.
 */

public class ForgotPasswordFragment extends Fragment {

    View view;
    AppCompatTextView loginLink;
    LoginActivity loginActivity;

    public ForgotPasswordFragment() {
    }

    public static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        loginActivity = (LoginActivity) getActivity();
        initialize();
        return view;
    }

    private void initialize() {
        loginLink = (AppCompatTextView) view.findViewById(R.id.login_link);

        SpannableString loginSpannable = new SpannableString("Booyah! I remember. Login");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                loginActivity.onClickLogin();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        loginSpannable.setSpan(clickableSpan, 19, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        loginLink.setText(loginSpannable);
        loginLink.setMovementMethod(LinkMovementMethod.getInstance());
        loginLink.setHighlightColor(Color.TRANSPARENT);
    }
}
