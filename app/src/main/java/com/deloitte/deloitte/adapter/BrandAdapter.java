package com.deloitte.deloitte.adapter;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deloitte.deloitte.MainActivity;
import com.deloitte.deloitte.R;
import com.deloitte.deloitte.model.pojo.readbrands.Brands;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Rats on 14/6/2017.
 */

public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.BrandViewHolder> {

    private final SortedList<Brands> mSortedList = new SortedList<>(Brands.class, new SortedList.Callback<Brands>() {
        @Override
        public int compare(Brands a, Brands b) {
            return mComparator.compare(a, b);
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(Brands oldItem, Brands newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(Brands item1, Brands item2) {
            return Integer.parseInt(item1.getId()) == Integer.parseInt(item2.getId());
        }
    });

    private final Comparator<Brands> mComparator;

    private AppCompatActivity context;

    public BrandAdapter(Comparator<Brands> comparator, AppCompatActivity context) {
        mComparator = comparator;
        this.context = context;
    }

    public void add(Brands model) {
        mSortedList.add(model);
    }

    public void remove(Brands model) {
        mSortedList.remove(model);
    }

    public void add(List<Brands> models) {
        mSortedList.addAll(models);
    }

    public void remove(List<Brands> models) {
        mSortedList.beginBatchedUpdates();
        for (Brands model : models) {
            mSortedList.remove(model);
        }
        mSortedList.endBatchedUpdates();
    }

    public void replaceAll(List<Brands> models) {
        mSortedList.beginBatchedUpdates();
        for (int i = mSortedList.size() - 1; i >= 0; i--) {
            final Brands model = mSortedList.get(i);
            if (!models.contains(model)) {
                mSortedList.remove(model);
            }
        }
        mSortedList.addAll(models);
        mSortedList.endBatchedUpdates();
    }

    @Override
    public BrandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.brand_view_item, parent, false);
        return new BrandViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BrandViewHolder holder, int position) {
        holder.name.setText(mSortedList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mSortedList.size();
    }

    class BrandViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        BrandViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.brand_name);
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (context instanceof MainActivity) {
                        ((MainActivity) context).onClick(mSortedList.get(getAdapterPosition()).getId());
                    }
                }
            });
        }
    }

}
