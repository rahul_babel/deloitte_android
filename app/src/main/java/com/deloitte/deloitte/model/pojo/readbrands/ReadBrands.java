package com.deloitte.deloitte.model.pojo.readbrands;

public class ReadBrands
{
    private String message;

    private Brands[] brands;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Brands[] getBrands ()
    {
        return brands;
    }

    public void setBrands (Brands[] brands)
    {
        this.brands = brands;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", brands = "+brands+", code = "+code+"]";
    }
}