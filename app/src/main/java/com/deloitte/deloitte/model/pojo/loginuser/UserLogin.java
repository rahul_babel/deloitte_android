package com.deloitte.deloitte.model.pojo.loginuser;

import java.io.Serializable;

public class UserLogin implements Serializable
{
    private String message;

    private String api_token;

    private String code;

    private User user;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getApi_token ()
    {
        return api_token;
    }

    public void setApi_token (String api_token)
    {
        this.api_token = api_token;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", api_token = "+api_token+", code = "+code+", user = "+user.toString()+"]";
    }
}