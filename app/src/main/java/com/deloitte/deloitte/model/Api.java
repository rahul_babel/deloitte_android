package com.deloitte.deloitte.model;

import com.deloitte.deloitte.model.pojo.CheckSerial;
import com.deloitte.deloitte.model.pojo.UserSignUp;
import com.deloitte.deloitte.model.pojo.loginuser.UserLogin;
import com.deloitte.deloitte.model.pojo.readbrands.ReadBrands;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Rats on 11/6/2017.
 */

public interface Api {

    @FormUrlEncoded
    @POST("login-user")
    Call<UserLogin> postLogin(@Field("phone") String phone, @Field("password") String password);

    @FormUrlEncoded
    @POST("create-user")
    Call<UserSignUp> postSignUp(@Field("email") String email, @Field("password") String password, @Field("name") String name, @Field("address") String address, @Field("phone") String phone);

    @GET("read-brands")
    Call<ReadBrands> getBrands(@Header("Authorization") String api_token);

    @FormUrlEncoded
    @POST("check-serial")
    Call<CheckSerial> postCheckSerial(@Header("Authorization") String api_token, @Field("brand_id") String brandId, @Field("serial_number") String serialNumber, @Field("location") String location);
}
