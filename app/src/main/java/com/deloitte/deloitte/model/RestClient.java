package com.deloitte.deloitte.model;

import com.deloitte.deloitte.utils.CustomConverter;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static Api REST_CLIENT;
    private static String ROOT = "http://192.168.1.102/api/";

    static {
        setupRestClient();
    }

    private RestClient() {
    }

    public static Api post() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        //to enable log
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        // end

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT)
                .addConverterFactory(CustomConverter.create())
                .client(httpClient.build())
                .build();

        REST_CLIENT = retrofit.create(Api.class);
    }
}
