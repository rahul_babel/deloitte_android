package com.deloitte.deloitte.model.pojo.readbrands;

public class Brands
{
    private String id;

    private String updated_at;

    private String name;

    private String created_at;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", updated_at = "+updated_at+", name = "+name+", created_at = "+created_at+"]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Brands model = (Brands) o;

        if (Integer.parseInt(id) != Integer.parseInt(model.id)) return false;
        return name != null ? name.equals(model.name) : model.name == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (Integer.parseInt(id) ^ (Integer.parseInt(id) >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}