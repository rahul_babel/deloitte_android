package com.deloitte.deloitte.model.pojo.loginuser;

import java.io.Serializable;

public class User implements Serializable {
    private String id;

    private String phone;

    private String updated_at;

    private String address;

    private String email;

    private String name;

    private String created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", phone = " + phone + ", updated_at = " + updated_at + ", address = " + address + ", email = " + email + ", name = " + name + ", created_at = " + created_at + "]";
    }
}