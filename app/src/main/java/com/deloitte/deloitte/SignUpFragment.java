package com.deloitte.deloitte;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SignUpFragment extends Fragment implements View.OnClickListener {

    View view;
    AppCompatTextView loginLink;
    AppCompatEditText emailEditText;
    AppCompatEditText passwordEditText;
    AppCompatEditText confirmPasswordEditText;
    AppCompatEditText nameEditText;
    AppCompatEditText addressEditText;
    AppCompatEditText phoneEditText;
    AppCompatButton signUpButton;
    LoginActivity loginActivity;

    public SignUpFragment() {
    }

    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        initialize();
        return view;
    }

    private void initialize() {
        loginLink = (AppCompatTextView) view.findViewById(R.id.login_link);
        loginActivity = (LoginActivity) getActivity();
        emailEditText = (AppCompatEditText) view.findViewById(R.id.email);
        passwordEditText = (AppCompatEditText) view.findViewById(R.id.password);
        confirmPasswordEditText = (AppCompatEditText) view.findViewById(R.id.confirm_password);
        nameEditText = (AppCompatEditText) view.findViewById(R.id.name);
        addressEditText = (AppCompatEditText) view.findViewById(R.id.address);
        phoneEditText = (AppCompatEditText) view.findViewById(R.id.phone);
        signUpButton = (AppCompatButton) view.findViewById(R.id.phone_sign_up_button);
        makeSpannable();
        signUpButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone_sign_up_button:
                if (validation())
                    loginActivity.getPresenter().postSignUp(emailEditText.getText().toString(), passwordEditText.getText().toString(),nameEditText.getText().toString(), addressEditText.getText().toString(), phoneEditText.getText().toString());
                break;
        }
    }

    private boolean validation() {
        emailEditText.setError(null);
        passwordEditText.setError(null);
        confirmPasswordEditText.setError(null);
        nameEditText.setError(null);
        phoneEditText.setError(null);

        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String confirmPassword = confirmPasswordEditText.getText().toString();
        String name = nameEditText.getText().toString();
        String phone = phoneEditText.getText().toString();

        View focusView = null;

        if (TextUtils.isEmpty(name)) {
            nameEditText.setError(getString(R.string.error_field_required));
            focusView = nameEditText;
            focusView.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.error_field_required));
            focusView = emailEditText;
            focusView.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(phone)) {
            phoneEditText.setError(getString(R.string.error_field_required));
            focusView = phoneEditText;
            focusView.requestFocus();
            return false;
        } else if (!isPhoneValid(phone)) {
            phoneEditText.setError(getString(R.string.error_invalid_phone));
            focusView = phoneEditText;
            focusView.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.error_field_required));
            focusView = passwordEditText;
            focusView.requestFocus();
            return false;
        }

        if (!isPasswordValid(password)) {
            passwordEditText.setError(getString(R.string.error_invalid_password));
            focusView = passwordEditText;
            focusView.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            confirmPasswordEditText.setError(getString(R.string.error_field_required));
            focusView = confirmPasswordEditText;
            focusView.requestFocus();
            return false;
        }

        if (!isPasswordValid(confirmPassword)) {
            confirmPasswordEditText.setError(getString(R.string.error_invalid_password));
            focusView = confirmPasswordEditText;
            focusView.requestFocus();
            return false;
        }

        if (!password.equals(confirmPassword)) {
            confirmPasswordEditText.setError(getString(R.string.error_invalid_password));
            focusView = confirmPasswordEditText;
            focusView.requestFocus();
            return false;
        }

        return true;
    }

    private boolean isPhoneValid(String phone) {
        return phone.matches("^(\\+91[\\-\\s]?)?[0]?(91)?[789]\\d{9}$");
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 3;
    }

    private void makeSpannable() {
        SpannableString loginSpannable = new SpannableString("Not a Newbie. Don't worry Login");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                loginActivity.onClickLogin();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        loginSpannable.setSpan(clickableSpan, 26, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        loginLink.setText(loginSpannable);
        loginLink.setMovementMethod(LinkMovementMethod.getInstance());
        loginLink.setHighlightColor(Color.TRANSPARENT);
    }
}
