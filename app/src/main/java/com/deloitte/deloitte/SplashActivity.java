package com.deloitte.deloitte;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.deloitte.deloitte.utils.PreferenceStorage;

/**
 * Created by Rats on 12/6/2017.
 */

public class SplashActivity extends AppCompatActivity {

    PreferenceStorage preferenceStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceStorage = PreferenceStorage.getInstance();
        if (preferenceStorage.getStringData("api_token") != null && !preferenceStorage.getStringData("api_token").isEmpty()) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
            Toast.makeText(this, "main", Toast.LENGTH_LONG).show();
        } else {
            Log.d("API_TOKEN", preferenceStorage.getStringData("api_token"));
            Toast.makeText(this, "login" + preferenceStorage.getStringData("api_token"), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
