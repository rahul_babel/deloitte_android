package com.deloitte.deloitte;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class LoginFragment extends Fragment implements View.OnClickListener {

    View view;
    AppCompatTextView signUpLink;
    AppCompatTextView forgotPasswordLink;
    AppCompatEditText phoneEditText;
    AppCompatEditText passwordEditText;
    AppCompatButton loginButton;
    LoginActivity loginActivity;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        initialize();
        return view;
    }

    private void initialize() {
        loginActivity = (LoginActivity) getActivity();
        signUpLink = (AppCompatTextView) view.findViewById(R.id.sign_up_link);
        phoneEditText = (AppCompatEditText) view.findViewById(R.id.phone);
        passwordEditText = (AppCompatEditText) view.findViewById(R.id.password);
        forgotPasswordLink = (AppCompatTextView) view.findViewById(R.id.forgot_password_link);
        loginButton = (AppCompatButton) view.findViewById(R.id.phone_sign_in_button);
        makeSpannable();
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone_sign_in_button:
                if (validation())
                    loginActivity.getPresenter().postLogin(phoneEditText.getText().toString(), passwordEditText.getText().toString());
                break;
        }
    }

    private boolean validation() {
        phoneEditText.setError(null);
        passwordEditText.setError(null);

        String phone = phoneEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        View focusView = null;

        if (TextUtils.isEmpty(phone)) {
            phoneEditText.setError(getString(R.string.error_field_required));
            focusView = phoneEditText;
            focusView.requestFocus();
            return false;
        } else if (!isPhoneValid(phone)) {
            phoneEditText.setError(getString(R.string.error_invalid_phone));
            focusView = phoneEditText;
            focusView.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.error_field_required));
            focusView = passwordEditText;
            focusView.requestFocus();
            return false;
        }

        if (!isPasswordValid(password)) {
            passwordEditText.setError(getString(R.string.error_invalid_password));
            focusView = passwordEditText;
            focusView.requestFocus();
            return false;
        }

        return true;
    }

    private boolean isPhoneValid(String phone) {
        return phone.matches("^(\\+91[\\-\\s]?)?[0]?(91)?[789]\\d{9}$");
    }

    private boolean isPasswordValid(String password) {
        return password.length() >=3;
    }

    private void makeSpannable() {
        SpannableString forgotPasswordSpannable = new SpannableString("Forgot Password?");
        SpannableString signUpSpannable = new SpannableString("Don't have an account? Sign Up");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if (textView.getId() == R.id.sign_up_link) {
                    loginActivity.onClickSignUp();
                } else {
                    loginActivity.onClickForgotPassword();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        forgotPasswordSpannable.setSpan(clickableSpan, 0, 16, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        signUpSpannable.setSpan(clickableSpan, 23, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        forgotPasswordLink.setText(forgotPasswordSpannable);
        forgotPasswordLink.setMovementMethod(LinkMovementMethod.getInstance());
        forgotPasswordLink.setHighlightColor(Color.TRANSPARENT);

        signUpLink.setText(signUpSpannable);
        signUpLink.setMovementMethod(LinkMovementMethod.getInstance());
        signUpLink.setHighlightColor(Color.TRANSPARENT);
    }
}
