package com.deloitte.deloitte;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.deloitte.deloitte.model.RestClient;
import com.deloitte.deloitte.model.pojo.loginuser.UserLogin;
import com.deloitte.deloitte.model.pojo.readbrands.ReadBrands;
import com.deloitte.deloitte.utils.ConnectionDetector;
import com.deloitte.deloitte.utils.Constants;
import com.deloitte.deloitte.utils.PreferenceStorage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rats on 14/6/2017.
 */

public class MainActivityPresenter {

    private AppCompatActivity context;
    private ReadBrands readBrands;
    private PreferenceStorage preferenceStorage;

    MainActivityPresenter(AppCompatActivity context) {
        this.context = context;
        preferenceStorage = PreferenceStorage.getInstance();
    }

    void getBrands() {
        if (ConnectionDetector.isConnectedToInternet()) {
            Call<ReadBrands> call = RestClient.post().getBrands(preferenceStorage.getStringData("api_token"));
            call.enqueue(new Callback<ReadBrands>() {
                @Override
                public void onResponse(Call<ReadBrands> call, Response<ReadBrands> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            readBrands = response.body();
                            if (readBrands.getCode().equals("0")) {
                                Toast.makeText(context, readBrands.getMessage(), Toast.LENGTH_LONG).show();
                            } else {
                                publish();
                            }
                        }
                    } else {
                        cancel();
                        Toast.makeText(context, Constants.SERVER_NOT_RESPONDING, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ReadBrands> call, Throwable t) {
                    cancel();
                    Toast.makeText(context, Constants.SERVER_NOT_RESPONDING, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            cancel();
            Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
        }
    }

    private void publish() {
        if (context instanceof MainActivity) {
            ((MainActivity) context).setAdapter(readBrands);
        }
    }

    private void cancel() {
        if (context instanceof MainActivity) {
            ((MainActivity) context).cancel();
        }
    }
}
