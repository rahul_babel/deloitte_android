package com.deloitte.deloitte;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.deloitte.deloitte.model.RestClient;
import com.deloitte.deloitte.model.pojo.UserSignUp;
import com.deloitte.deloitte.model.pojo.loginuser.UserLogin;
import com.deloitte.deloitte.utils.ConnectionDetector;
import com.deloitte.deloitte.utils.Constants;
import com.deloitte.deloitte.utils.PreferenceStorage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class LoginActivityPresenter {
    private AppCompatActivity context;
    private UserLogin userLogin;
    private UserSignUp userSignUp;
    private PreferenceStorage preferenceStorage;

    LoginActivityPresenter(AppCompatActivity context) {
        this.context = context;
        preferenceStorage = PreferenceStorage.getInstance();
    }

    void postLogin(String phone, String password) {
        if (ConnectionDetector.isConnectedToInternet()) {
            Call<UserLogin> call = RestClient.post().postLogin(phone, password);
            call.enqueue(new Callback<UserLogin>() {
                @Override
                public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            userLogin = response.body();
                            if (userLogin.getCode().equals("0")) {
                                Toast.makeText(context, userLogin.getMessage(), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(context, userLogin.getMessage(), Toast.LENGTH_LONG).show();
                                preferenceStorage.saveStringData("api_token", userLogin.getApi_token());
                                preferenceStorage.saveStringData("userId", userLogin.getUser().getId());
                                preferenceStorage.saveStringData("userName", userLogin.getUser().getName());
                                preferenceStorage.saveStringData("userEmail", userLogin.getUser().getEmail());
                                preferenceStorage.saveStringData("userPhone", userLogin.getUser().getPhone());
                                preferenceStorage.saveStringData("userAddress", userLogin.getUser().getAddress());
                                preferenceStorage.saveStringData("userCreatedAt", userLogin.getUser().getCreated_at());
                                preferenceStorage.saveStringData("userUpdatedAt", userLogin.getUser().getUpdated_at());
                                publishLogin();
                            }
                        }
                    } else {
                        Toast.makeText(context, Constants.SERVER_NOT_RESPONDING, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<UserLogin> call, Throwable t) {
                    Toast.makeText(context, Constants.SERVER_NOT_RESPONDING, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
        }
    }

    private void publishLogin() {
        if (context instanceof LoginActivity) {
            ((LoginActivity) context).login(userLogin);
        }
    }

    void postSignUp(String email, String password, String name, String address, String phone) {
        if (ConnectionDetector.isConnectedToInternet()) {
            Call<UserSignUp> call = RestClient.post().postSignUp(email, password, name, address, phone);
            call.enqueue(new Callback<UserSignUp>() {
                @Override
                public void onResponse(Call<UserSignUp> call, Response<UserSignUp> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            userSignUp = response.body();
                            if (userSignUp.getCode().equals("0")) {
                                Toast.makeText(context, userSignUp.getMessage(), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(context, userSignUp.getMessage(), Toast.LENGTH_LONG).show();
                                publishSignUp();
                            }
                        }
                    } else {
                        Toast.makeText(context, Constants.SERVER_NOT_RESPONDING, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<UserSignUp> call, Throwable t) {
                    Toast.makeText(context, Constants.SERVER_NOT_RESPONDING, Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(context, Constants.NO_INTERNET_CONNECTION, Toast.LENGTH_LONG).show();
        }
    }

    private void publishSignUp() {
        if (context instanceof LoginActivity) {
            ((LoginActivity) context).signUp(userSignUp);
        }
    }
}
