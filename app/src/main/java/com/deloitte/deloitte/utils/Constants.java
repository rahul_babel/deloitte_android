package com.deloitte.deloitte.utils;

/**
 * Created by Rats on 11/6/2017.
 */

public class Constants {
    public static String NO_INTERNET_CONNECTION = "No Internet Connection. Please Try Again.";
    public static String SERVER_NOT_RESPONDING = "We are unable to handle your request now. Please try again shortly";
    public static String SHARED_PREFERENCES_NAME = "deloitte.app";
    public static String LOGOUT_ERROR = "Error logging out. Please try again.";
    public static String LOGOUT_SUCCESS = "Successfully logged out";
}
