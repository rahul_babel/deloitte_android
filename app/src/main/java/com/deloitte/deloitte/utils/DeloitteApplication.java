package com.deloitte.deloitte.utils;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Rats on 11/6/2017.
 */

public class DeloitteApplication extends Application {
    public static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        Fabric.with(this, new Crashlytics());

    }

    public static Context getContext() {
        return context;
    }
}
