package com.deloitte.deloitte;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.deloitte.deloitte.adapter.BrandAdapter;
import com.deloitte.deloitte.model.pojo.readbrands.Brands;
import com.deloitte.deloitte.model.pojo.readbrands.ReadBrands;
import com.deloitte.deloitte.utils.Constants;
import com.deloitte.deloitte.utils.PreferenceStorage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener, SwipeRefreshLayout.OnRefreshListener {

    AppCompatTextView userName;
    AppCompatTextView userEmail;
    NavigationView navigationView;
    PreferenceStorage preferenceStorage;
    private BrandAdapter mAdapter;
    private List<Brands> mModels;
    private RecyclerView recyclerView;
    MainActivityPresenter mainActivityPresenter;
    SwipeRefreshLayout swipeRefreshLayout;
    SearchView searchView;

    private static final Comparator<Brands> ALPHABETICAL_COMPARATOR = new Comparator<Brands>() {
        @Override
        public int compare(Brands a, Brands b) {
            return a.getName().compareTo(b.getName());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferenceStorage = PreferenceStorage.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        initialize();
    }

    private void initialize() {
        userName = (AppCompatTextView) navigationView.getHeaderView(0).findViewById(R.id.user_name);
        userEmail = (AppCompatTextView) navigationView.getHeaderView(0).findViewById(R.id.user_email);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        userName.setText(preferenceStorage.getStringData("userName"));
        userEmail.setText(preferenceStorage.getStringData("userEmail"));
        mAdapter = new BrandAdapter(ALPHABETICAL_COMPARATOR, this);
        recyclerView = (RecyclerView) findViewById(R.id.brand_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
        mModels = new ArrayList<>();
        mainActivityPresenter = new MainActivityPresenter(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        mainActivityPresenter.getBrands();
                                    }
                                }
        );
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_logout) {
            if (preferenceStorage.removeKey("api_token")) {
                Toast.makeText(this, Constants.LOGOUT_SUCCESS, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, Constants.LOGOUT_ERROR, Toast.LENGTH_LONG).show();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<Brands> filteredModelList = filter(mModels, query);
        mAdapter.replaceAll(filteredModelList);
        recyclerView.scrollToPosition(0);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        mainActivityPresenter.getBrands();
    }

    private static List<Brands> filter(List<Brands> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<Brands> filteredModelList = new ArrayList<>();
        for (Brands model : models) {
            final String text = model.getName().toLowerCase();
            if (text.contains(lowerCaseQuery)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    public void setAdapter(ReadBrands readBrands) {
        swipeRefreshLayout.setRefreshing(false);
        mModels.addAll(Arrays.asList(readBrands.getBrands()));
        searchView.setQuery("", false);
        onQueryTextChange("");
    }

    public void cancel() {
        swipeRefreshLayout.setRefreshing(false);
    }

    public void onClick(String brandId) {
        Intent intent = new Intent(this, CaptureSerialActivity.class);
        intent.putExtra("brand_id", brandId);
        startActivity(intent);
    }
}
